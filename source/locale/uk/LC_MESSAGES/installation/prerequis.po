# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2011-2020, Johan Cwiklinski
# This file is distributed under the same license as the Galette package.
# Weblate <noreply@weblate.org>, 2020.
# Tymofij Lytvynenko <till.svit@gmail.com>, 2021.
# Johan Cwiklinski <trasher@x-tnd.be>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Galette 0.9.4\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-08 20:13+0200\n"
"PO-Revision-Date: 2023-12-09 14:09+0000\n"
"Last-Translator: Johan Cwiklinski <trasher@x-tnd.be>\n"
"Language-Team: Ukrainian <https://hosted.weblate.org/projects/galette/doc-"
"install-prerequisites/uk/>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 5.3-dev\n"

# ddb7105580ce4f5cb1da700a8e054567
#: ../installation/prerequis.rst:7 ddb7105580ce4f5cb1da700a8e054567
msgid "Prerequisites and hosting"
msgstr "Передумови та хостинг"

# 8ebab4ca7c2d4d6c8ffc5e1d273afc26
#: ../installation/prerequis.rst:9 8ebab4ca7c2d4d6c8ffc5e1d273afc26
msgid "To install Galette, you will need to meet the following requirements :"
msgstr "Для установлення Galette вам необхідно відповідати наступним вимогам:"

# da519b3596e94a2fa5e9762dbece91fe
#: ../installation/prerequis.rst:11 da519b3596e94a2fa5e9762dbece91fe
msgid "a web server (like Apache),"
msgstr "вебсервер (наприклад, Apache),"

# 4f3be612bd1045fda0ce26abe907551a
#: ../installation/prerequis.rst:12 4f3be612bd1045fda0ce26abe907551a
msgid "PHP 8.1 or more recent,"
msgstr "PHP 8.1 або новішої версії,"

# 8871e287e06e42f38f7e98c6c784cdd7
#: ../installation/prerequis.rst:14 8871e287e06e42f38f7e98c6c784cdd7
msgid "`gd` PHP module,"
msgstr "PHP-модуль `gd`,"

# 6746889eee8744389edd341b1062560a
#: ../installation/prerequis.rst:15 6746889eee8744389edd341b1062560a
msgid "`PDO` module and `mysql` ou `postgresql` for PDO,"
msgstr "модуль `PDO` і `mysql` на `postgresql` для PDO,"

# bacb3299a33544629d7cfd90f26f7866
#: ../installation/prerequis.rst:16 bacb3299a33544629d7cfd90f26f7866
msgid "`curl` PHP module,"
msgstr "PHP-модуль `curl`,"

# 88151f2cf90d44809fd0a5e044a773a4
#: ../installation/prerequis.rst:17 88151f2cf90d44809fd0a5e044a773a4
msgid "`intl` PHP module,"
msgstr "PHP-модуль `intl`,"

# d8e2cfeca8434001ba0e9d9aa37ec0a6
#: ../installation/prerequis.rst:18 d8e2cfeca8434001ba0e9d9aa37ec0a6
msgid "SSL support,"
msgstr "підтримка SSL,"

# d6cb23e1d90341c3a5e4c94c378e0648
#: ../installation/prerequis.rst:19 d6cb23e1d90341c3a5e4c94c378e0648
msgid "`gettext` PHP module (optional)."
msgstr "PHP-модуль `gettext` (необов'язково)."

# 07ced233b53a42dcbb05c6ca06122f55
#: ../installation/prerequis.rst:21 07ced233b53a42dcbb05c6ca06122f55
msgid ""
"A database server, `MariaDB <https://mariadb.org>`_ 10.4 minimum (or MySQL "
"5.7 minimum), or `PostgreSQL <https://postgresql.org>`_ 11 minimum."
msgstr ""
"Сервер бази даних, `MariaDB <https://mariadb.org>`_ 10.4 мінімально (чи "
"MySQL 5.7 мінімально), чи`PostgreSQL <https://postgresql.org>`_ 11 "
"мінімально."

# 527b090fb9114e3fb6efc59f106820fa
#: ../installation/prerequis.rst:23 527b090fb9114e3fb6efc59f106820fa
msgid ""
"Galette is tested continuously with recent versions of these components. If "
"you encounter issues with a recent version, please let us know ;)"
msgstr ""
"Galette постійно тестується з останніми версіями цих складників. Якщо у вас "
"виникли проблеми з останньою версією, повідомте нас про це;)"

# d495e8cfd052458da2083c64e7344184
#: ../installation/prerequis.rst:25 d495e8cfd052458da2083c64e7344184
msgid "Galette does not work on the following hostings:"
msgstr "Galette не працює в наступних хостингах:"

# 9ed65c8222a64b678a39a3f99c826627
#: ../installation/prerequis.rst:27 9ed65c8222a64b678a39a3f99c826627
msgid "Free,"
msgstr "Безплатно,"

# 928a21ff8870444689905d3223783dec
#: ../installation/prerequis.rst:28 928a21ff8870444689905d3223783dec
msgid "Olympe Networks."
msgstr "Olympe Networks."

# 2519703ee2604b55a98654c435a8552a
#~ msgid "`tidy` PHP module (optional, but recommended),"
#~ msgstr "PHP-модуль `tidy` (необов'язково, але пораджено),"
